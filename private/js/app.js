'use strict';

import memberId from './functions/memberId';
import scrollTo from './functions/scrollTo';
import tracking from './functions/tracking';
import moment from './functions/moment';
import link from './functions/link';
import scrollAnimations from './functions/scrollAnimations';
import sliders from './functions/sliders';
import resizeYT from './functions/resizeYT';
import devTool from './functions/devTool'
import tooltips from './functions/tooltips'
import marketing from './functions/marketing'
import header from './functions/header'
import travel from "./functions/travel";
import form from "./functions/form";
import addEmail from "./functions/addEmail";
import vhFixed from "./functions/vhFixed";
import scrollInfo from "./functions/scrollinfo";

$(function () {

    /* MemberId catching and loader display */
    memberId();

    tooltips('.event');

    scrollTo('.scrollTo');

    tracking();

    moment();

    link();

    marketing();

    scrollAnimations();

    sliders();

    resizeYT();

    devTool();

    header();

    travel();

    form();

    addEmail();

    vhFixed();

    scrollInfo();



});


/* TITLE LETTER SPLIT IN SPANS */

$('.titlearea .highlight').each(function() {
    var html = $(this).html();
    var ret  = "";

    $.each(html.split(''), function(k, v) {


        ret += "<span class='letter letter--"+k+"'>" + v + "</span>";
    });

    $(this).html(ret);

    $('.letter').each(function() {
        if( $(this).html() == ' ' ) {
            $(this).addClass('empty')
        }
    })


})








/*var bgPos = $('.bgmove').css('background-position-x');

$('.bgmove').each(function() {

    $(window).scroll(function() {

        var theta = $(window).scrollTop() / 750,
            test;


        if( $(this).hasClass('bgmove--reverse') ) {
            test= parseInt(bgPos) * -theta;
        }else{
            test= parseInt(bgPos) * theta;
        }

        $(this).css({ 'background-position-x': + test });


    });
})*/

