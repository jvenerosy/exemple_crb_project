'use strict';

import getURLParams from './getURLParams';
import loader from './loader';

export default function (qs) {
    var query = window.location.search.substring(1);

    // Don't forget to add ".minisiteLink" class on every link to another page of the minisite
    if(query.length)
        $('.minisiteLink').each(function (i, e) {
            $(e).attr('href', $(e).attr('href')+'?'+query);
        });
    var qs = getURLParams(query);
    let member_id = propriet['Member ID'];

    let hasMemberID = (member_id !== "" && member_id !== undefined && member_id !== null);
    let data=null;

    delete require.cache[require.resolve("../../data/config")];
    let configSite = require("../../data/config");

    Object.keys(configSite).map(lang => {

        data = configSite[lang];
        if (!hasMemberID && data.modules.autologin === true) {
            member_id = 'test';
        }
    });


    if(!hasMemberID && data!==null && data.modules.autologin === true && (qs.test===undefined)) {
        window.top.location = "https://secure.fr.vente-privee.com";
    } else {
        //$('body').removeClass('bypassed');
        loader();
    }

    //Hashage member Id and put the value in the aliasMemberId variable + alias for Mixpanel
    if(data.modules.hashMemberId == true) {
        let aliasMemberId,
            apiUrl = config.environment === 'development' ? data.mixpanel.preprod.apiHashMemberId : data.mixpanel.prod.apiHashMemberId;
            apiUrl += config.operation.code;

        let param = {member_id: member_id};

        $.post(apiUrl, param, function(result, status) {

            if(status == "success") {
                aliasMemberId = result.partner_id;
                mixpanel.alias(aliasMemberId);

                $(".hash-memberid").each(function () {
                    let link = $(this).attr("href");

                    // Prepend "&" or "?" to the hash if the link already has a query string or not
                    let prefix = link.indexOf('?') ? "?" : "&";

                    $(this).attr("href", link + prefix + "memberId=" + aliasMemberId);
                });
            }
        });

    }
}