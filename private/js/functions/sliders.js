'use strict';

export default function (trigger) {


    $('.responsive').slick({
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        centerMode: true,
        centerPadding: '20px',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    dots: true,
                    slidesToShow: 1,
                    centerMode: true,
                    autoplay: true,
                    autoplaySpeed: 3000,
                    speed: 1500

                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });


    $('.collec__slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        if( $(window).width() < 640 ) {
            $('.collec__buttons-col').removeClass('on')
            $('.collec__buttons-col').eq(nextSlide).addClass('on')
        }

    });




}
