'use strict';

export default function(){

    function closeLoader() {
        $(".loader").fadeOut("100");
        setTimeout(function() {
            $('.intro__promo, .intro').addClass('anim');
        }, 1000)

    }

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function checkCookie() {

        let loaderactif = getCookie("loaderactif");
        if (loaderactif != "true") {

            document.cookie = "loaderactif=true";
            setTimeout(function(){
                closeLoader();
            }, 4500);
        } else {
            closeLoader();
        }
    }

    checkCookie();

}
