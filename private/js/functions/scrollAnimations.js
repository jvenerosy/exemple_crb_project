'use strict';

export default function(dest){

    $('.intro').waypoint(function (direction) {
        if (direction === 'down') {
            $('.intro, .intro__promo-txt.titlearea__title').removeClass('anim');
        }
    }, {
        offset: '-10'
    });

    $('.rooms').waypoint(function (direction) {
        if (direction === 'up') {
            $('.intro, .intro__promo-txt.titlearea__title').addClass('anim');
        }
    }, {
        offset: '60%'
    });

    $('.collec .titlearea').waypoint(function (direction) {
        if (direction === 'up') {
            $('.collec').removeClass('anim');
        }else {
            $('.collec').addClass('anim');
        }
    }, {
        offset: '70%'
    });

    $('.vid .titlearea').waypoint(function (direction) {
        if (direction === 'up') {
            $('.vid').removeClass('anim');
        }else {
            $('.vid').addClass('anim');
        }
    }, {
        offset: '70%'
    });

    setTimeout(function() {
        $('#player').waypoint(function (direction) {
            if (direction === 'up') {
                stopVideo()
            }else {
                playVideo()
            }
        }, {
            offset: '70%'
        });
    }, 300)



    $('.rooms__slider').waypoint(function (direction) {
        if (direction === 'up') {
            $('.rooms__slider').removeClass('anim');
        }else {
            $('.rooms__slider').addClass('anim');
        }
    }, {
        offset: '70%'
    });

    /* HORIZONTAL TITLE SCROLL */
    var scrollPos = 0;
    window.addEventListener('scroll', function(){
        if ((document.body.getBoundingClientRect()).top > scrollPos) {
            // UP
            $('.collec__slide-title').css({ 'left': (this.scrollY / -60) + '%'});
        }  else {
            // DOWN
            scrollPos = (document.body.getBoundingClientRect()).top;
            $('.collec__slide-title').css({ 'left': (this.scrollY / -60) + '%'});
        }

    });



}
