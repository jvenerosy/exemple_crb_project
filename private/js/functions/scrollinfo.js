'use strict';

export default function(){
   
    let display = function() {
         
        let docH = $(document).height();
        let winH = $(window).height();
        let scrollTop = $(window).scrollTop();

        let percent = (scrollTop / (docH - winH)) * 100;

        $('.scrollInfo__percent').width(percent + '%');
    };

    display();

    $(window).scroll(display).resize(display);

}