
export default function(){
    // axios.get("https://tcross-volkswagen-pp-vow.as44099.com/api/pre-order/vehicles/available", {}, {
    //     headers: {
    //         'Access-Control-Request-Headers' : '*',
    //         'Content-Type': 'application/json',
    //     },
    // })
    // .then(
    //     (response)=>{
    //         console.log('get vehicule : ', response);

    //         $('#vehiculeVWnbr').text(response.toString());
    //     },
    //     (err)=>{
    //         console.log('err get vehicule : ', err);
    //     }
    // )
    $.ajax ({
        type: 'GET',
        url : "http://brand-mail-api-preprod.front.vpgrp.io/opespe/im_vw2",
        // crossDomain: true,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        beforeSend: function (jqxhr) {
         jqxhr.setRequestHeader('Content-type', 'application/json');
         jqxhr.setRequestHeader('Accept', 'application/json');
       },
        success:   function  (response)  {
            console.log('get vehicule : ', response);

            $('#vehiculeVWnbr').text(JSON.parse(response.data).vehicles.toString());
        },
        complete:   function (xhr) {
            console.log('complete get vehicule : ', xhr);
        },
        error:   function  (xhr)  {
            console.log('err get vehicule : ', xhr);
        }
    })
}