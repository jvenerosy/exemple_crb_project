'use strict';

export default function (e) {


    $('.form__input--submit').on('click', function (e) {
        e.preventDefault();

        var lastname = $('#lastname').val(),
            firstname = $('#firstname').val(),
            mail = $('#mail').val();

        var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

        var rules = $('#rules').val();

        var offers = $('#offers').val();


        if(!testEmail.test(mail)){
            $('#mail').closest('.form__inputwrap').addClass("error");
        }
        if( !$('.form__inputwrap--check').hasClass('checked') ) {
            $('#rules').closest('.form__inputwrap').addClass("error");
        }

        $('.form__input--text').each(function() {
            if( $(this).val() == '') {
                $(this).closest('.form__inputwrap').addClass("error");
            }
        })

        if( testEmail.test(mail) && $('.form__inputwrap--check').hasClass('checked') && $('#lastname').val() != '' && $('#firstname').val() != '' ){
            $.ajax ({
                type: 'POST',
                headers: {
                    'Content-Type':'application/x-www-form-urlencoded'
                },
                //url: 'https://brandapi.vpfront.com/api/projects/'+ trad.project.toLowerCase() +'/users',
                url: 'https://brandapi.preprod.vpfront.vpgrp.io/api/projects/'+ trad.project.toLowerCase() +'/users',
                data: {"email" : mail, "data" : {'lastname': lastname,'firstname': firstname,'optinRules': rules,'optinOffers': offers} },
                success:   function  (data)  {
                    console.log('success', data);
                },
                complete:   function (xhr) {

                    console.log('complete', xhr);

                    // MIXPANEL DATA SEND "on quiz submit"
                    var libell = 'submit-form-ok';
                    mixpanel.track("Click", {
                        "Operation Code": config.operation.code,
                        "Click Name": "Mini site " + libell
                    });

                    $('.form__inner').addClass('off');
                    $('.form__result').addClass('on');

                    setTimeout(function () {
                        $('.form__inputwrap--check').removeClass('checked');
                        $('.form__inputwrap').removeClass('error full');
                        $('.form__input').val('');
                        $('.form__inner').removeClass('off');
                        $('.form__result').removeClass('on');
                    }, 3000)

                },
                error:   function  (xhr)  {
                    console.log('error', xhr)
                }
            })
        }

    })
}