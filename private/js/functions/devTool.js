"use strict";

export default function() {

    let getURLParameter = function(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
    };

    let openDevToolbar = function() {
        headerDev.removeClass('header-dev--closed');
    };

    let closeDevToolbar = function() {
        headerDev.addClass('header-dev--closed');
    };

    let openAppPopin = function() {
        $('.app-popin').removeClass('app-popin--closed');
    };


    let headerDev = $('.header-dev');
    let hash = window.location.hash;


    headerDev.find('button.header-dev__mixpanel').on('click', function(){

        let mixpanel = getURLParameter('mp_properties');
        let url;

        if(!mixpanel) {
            let token = 'eyJJbnRlcmZhY2UiOiJTbWFydHBob25lIEFuZHJvaWQgYXBwIiwiVVRNIFRlcm0gW0xhc3QgVG91Y2hdIjoiT3JnYW5pYyIsIlVUTSBDb250ZW50IFtMYXN0IFRvdWNoXSI6Ik9yZ2FuaWMiLCJVVE0gQ2FtcGFpZ24gW0xhc3QgVG91Y2hdIjoiT3JnYW5pYyIsIlVUTSBNZWRpdW0gW0xhc3QgVG91Y2hdIjoiT3JnYW5pYyIsIlVUTSBTb3VyY2UgW0xhc3QgVG91Y2hdIjoiT3JnYW5pYyIsIkNSTSBTZWdtZW50IjoxLCIjIG9mIENvbXBsZXRlZCBQdXJjaGFzZXMiOjI1NiwiTWVtYmVyIElEIjoxNTc3Mzk4NiwiUG9pbnQgcmVhY2hlZCBvbiBob21lcGFnZSI6NCwiT3B0aW1pemVseSBDdXJyZW50IEFcL0IgVGVzdHMiOlsiNzg3Njk0MDAxNCAtIFZhcmlhdGlvbiBSYW5kb20iXSwiU291cmNlIjoiT3JnYW5pYyIsIlBvaW50IFJlYWNoZWQgT24gSG9tZXBhZ2UiOjR9';
            url = window.location.host + '?mp_properties=' + token;
        } else {
            url = window.location.host;
        }

        window.location = 'http://' + url + hash;

    });


    headerDev.find('button.header-dev__swap').on('click', function(){

        let swap = window.location.hash;

        let url;

        if(swap !== '#swap') {
            url = window.location.host + '#swap';
            window.location.href = 'http://' + url;
            location.reload();
        } else {
            url = window.location.host;
            window.location.href = 'http://' + url;
        }


    });


    headerDev.find('button.header-dev__tracking').on('click', function(){

        let tracking = window.location.hash;

        let url;

        if(tracking !== '#tracking') {
            url = window.location.host + '#tracking';
            window.location.href = 'http://' + url;
            location.reload();
        } else {
            url = window.location.host;
            window.location.href = 'http://' + url;
        }

    });


    headerDev.find('.header-dev__close').on('click', function(){
        closeDevToolbar();
    });


    headerDev.find('.header-dev__open-app').on('click', function(){

        let url = window.location.href;
        window.location.href = 'appvp://openwv/' + encodeURIComponent(url);

    });


    // Open devToolbar when hash is #dev
    if (hash === '#dev') {
        openDevToolbar();
    }

    // Show a popin to open mini site in VP App
    if (hash === '#app') {

        openAppPopin();

        let miniSiteUrl = window.location.protocol + '//' + window.location.host + window.location.pathname;

        $('.js-close-app-popin').on('click', function() {
            window.location.href = miniSiteUrl;
        });

        $('.js-open-app').on('click', function() {
            window.location.href = 'appvp://openwv/' + encodeURIComponent(miniSiteUrl);
        });

    }

}
