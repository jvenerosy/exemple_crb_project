'use strict';

export default function (qs) {
    qs = qs.split('+').join(' ');

    var params = {},
        tokens,
        re = /[?&]?([^=]+)/g,
        re2 = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs))
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);

    while (tokens = re2.exec(qs))
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);


    return params;
}