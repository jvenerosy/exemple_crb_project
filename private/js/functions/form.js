'use strict';


export default function (trigger) {

    // LABEL ANIMATION ON FOCUS
    $('.form__input').not('.form__input--checkbox').on('focus', function() {
        $(this).closest('.form__inputwrap').addClass('focus');
    });

    // LABEL ANIMATION ON BLUR
    $('.form__input').on('blur', function() {
        if( $(this).val() == '' ) {
            $(this).closest('.form__inputwrap').removeClass('focus');
        }

    });


    $('.form__inputwrap--check').on('click', function() {
        $(this).toggleClass('checked full');
        if(  $(this).hasClass('checked') ) {
            $(this).find('input').attr('value', true)
        }else {
            $(this).find('input').attr('value', false)
        }
    })

    // On keyup fields, addclass "full" and increment counter for knob
    function onkeyUpInputs(elem) {
        $(elem).on('keyup', function() {
            if( $(elem).val() != '' ) {
                $(elem).closest('.form__inputwrap').addClass('full').removeClass('error');
            }else {
                $(elem).closest('.form__inputwrap').removeClass('full');
            }
        })
    }

    var inputs      = $('.form__input').not('[type="submit"]');
    inputs.each(function(index, elem) {
        onkeyUpInputs(elem);
    });

}
