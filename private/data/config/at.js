module.exports = {
    "url": {
        "normal": "http://secure.de.vente-privee.com/vp4/_sales/MINISITES1/opeSpe/",
        "secure": "https://secure.de.vente-privee.com/vp4/_sales/MINISITES1/opeSpe/",
        "desktop": "https://secure.de.vente-privee.com/homev6/de/Operation/Access/",
        "device": "https://m.vente-privee.com/w2/index.html#operation/",
        "application": "appvp://operation/",
        "oldApp": "applivp://OperationAccess/operationid/",
        "productLinkWeb": "https://secure.de.vente-privee.com/ns/de-at/operation/",
        "productLinkMobile": "https://m.vente-privee.com/w2/#operation/",
        "productLinkApp": "appvp://fp/",
        "token":"e1fcf2661096212f64500ac5c70b6956",
        "partner": "http://www.etam.com/"
    },
    "operation": {
        "code": "CODE_OPERATION",
        "id": "58921",
        "partnerID": "18739",
        "date": "27/09/2016 22:00",
        "titre": "METTRE UN TITRE",
        "description": "AINSI QU'UNE DESCRIPTION"
    },
    "marketingButton": {
        "beforeSale": "S’inscrire à la vente",
        "duringSale": "Accéder à la vente"
    },
    "twitter": {
        "message": "C'est trop un site rigolo #LOL"
    },
    "facebook": {
        "title": "Ceci est un titre Facebook",
        "description": "Ceci est une description Facebook"
    },
    "xtsite": {
        "appIphone": "538746",
        "appIpad": "538755",
        "appAandroidSmartphone": "538756",
        "appAndroidTablet": "563562",
        "smartphone": "495301",
        "default": "461117"
    }
}