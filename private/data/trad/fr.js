module.exports = {
    "project": "alinea",
    "intro": {
        "title": "alinea",
        "promo": {
            "offer": "-20% avec le code vp19",
            "intro": "Alinea dévoile",
            "txt": "deux nouvelles histoires <span class='highlight'>printanières</span>",
            "story1": "Samarcande",
            "story1_url": "https://www.alinea.com/fr-fr/media/histoire-samarcande.html?utm_source=vente-privee&utm_medium=Display&utm_campaign=vente-privee-03-2019",
            "story2": "San Remo",
            "_and": "et",
            "story2_url": "https://www.alinea.com/fr-fr/media/histoire-sanremo.html?utm_source=vente-privee&utm_medium=Display&utm_campaign=vente-privee-03-2019",
        },
        "cta": "je découvre"
    },
    "rooms": [
        {
            "title": "les tables et chaises",
            "link": "https://www.alinea.com/offre/VP0319repas",
        },
        {
            "title": "les canapés",
            "link": "https://www.alinea.com/offre/VP0319canapes",
        },
        {
            "title": "la chambre",
            "link": "https://www.alinea.com/offre/VP0319chambre",
        }
    ],
    "living": {
        "txt": "<strong>un brin de légèreté rétro </strong><br />pour le salon",
        "cta": "je m'inspire",
        "cta_url": "https://www.alinea.com/offre/VP0319salon"
    },
    "collec": {
        "intro": "faites un break et plongez dans la fraîcheur de ",
        "title": "notre collection de jardin <span class='highlight'>printemps 2019</span>",
        "slider": [
            {
                "title": "le jardin",
                "link": "https://www.alinea.com/offre/VP0319jardin",
            },
            {
                "title": "les cache-pots",
                "link": "https://www.alinea.com/offre/VP0319pots",
            },
            {
                "title": "les salons de jardin",
                "link": "https://www.alinea.com/offre/VP0319salonjardin",
            }
        ]
    },
    "video": {
        "title": "<span class='highlight'>San Remo</span>",
        "sub": "l’audace du design des années 70",
        "yt": {
            "media_url": "97UYWve1HKI",
        },
        "txt": "Tout le charme rétro de la Dolce Vita s’incarne en San Remo, une collection élégante, légère et moderne, qui se joue avec audace des codes design des 70’s. Roses poudrés et reflets dorés habillent meubles et accessoires. L’univers est minéral et les matières feutrées. Douceur de vivre insouciante, dans un décor chic et ensoleillé."

    },
    "marketing": {
        "txt1": "-20% avec le code VP19",
        "cta": "j'en profite",
        "cta_url": "https://www.alinea.com/offre/VP0319",
    },
    "mentions": "*Offre valable du 9 au 11/03/2019 sur alinea.com et en magasin (selon ouverture) sur présentation de l'offre. <br />Hors restauration, cartes cadeaux, librairie, services (montage, pose et livraison). Offre non cumulable avec d'autres promotions, offres ou avantages en cours. Dans la limite des stocks disponibles.",
    "footer_rights": "Tous droits réservés.",
};